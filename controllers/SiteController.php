<?php

namespace app\controllers;

use yii\data\ArrayDataProvider;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends \yii\web\Controller
{
    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $searchModel = new \app\models\search\ExchangeSearch();
        $models = $searchModel->search();

        $provider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => ['value' => SORT_DESC],
                'attributes' => ['currency', 'value'],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $provider,
        ]);
    }
}
