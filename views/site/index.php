<?php

/* @var $this yii\web\View */
/* @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = 'Test';

echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'containerOptions' => ['style'=>'overflow: auto'],
]);