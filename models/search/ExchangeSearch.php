<?php

namespace app\models\search;

/**
 * Class ExchangeSearch
 * @package app\models\search
 */
class ExchangeSearch extends \yii\base\Model
{
    /** @var string $currency */
    public $currency;
    /** @var float $price */
    public $price;

    /**
     * Ищем цены со всех подключенных бирж.
     *
     * @return array
     * @throws \Exception
     */
    public function search()
    {
        $externalApi = \Yii::$app->integrator->getExternalApi();
        $combineExchange = new \app\models\CombineExchange($externalApi);

        $list = [];
        foreach ($combineExchange->getCombinedPrices() as $prices) {
            foreach ($prices as $index => $price) {
                $list[] = [
                    'currency' => $index,
                    'value' => $price,
                ];
            }
        }

        return $list;
    }
}