<?php

namespace app\models;

/**
 * Композиция бирж для возвращения цен.
 *
 * Class CompositeExchange
 * @package app\models
 */
class CombineExchange
{
    /** @var array $exchanges */
    public $exchanges = [];

    /**
     * CompositeExchange constructor.
     * @param array $exchanges
     */
    public function __construct(array $exchanges)
    {
        $this->exchanges = $exchanges;
    }

    /**
     * Получить цены с подключеных бирж.
     *
     * @return array
     */
    public function getCombinedPrices()
    {
        $list = [];
        /** @var ExchangeInterface $exchange */
        foreach ($this->exchanges as $exchange) {
            $list[] = $exchange->getPrices();
        }

        return $list;
    }
}