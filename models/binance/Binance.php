<?php

namespace app\models\binance;

/**
 * Class Binance
 * @package app\models\binance
 */
class Binance extends \app\models\Exchange
{
    /**
     * @return array
     * @throws \Exception
     */
    public function getPrices()
    {
        /** @var \Binance\API $exchange */
        $exchange = $this->exchange;
        return $exchange->prices();
    }
}