<?php

namespace app\components;

/**
 * Компонент интеграции к внешним API бирж.
 *
 * Class Integrator
 * @package app\components
 */
class Integrator extends \yii\base\Component
{
    /** @var array $exchanges */
    public $exchanges;
    /** @var array $externalApi */
    public $externalApi = [];

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        foreach ($this->exchanges as $index => $exchange) {
            $class = $exchange['class'];
            $api = new $index($exchange['apiKey'], $exchange['secretKey']);
            // Получаем класс "обертку" для работы с биржей.
            $this->externalApi[] = new $class($api);
        }
    }

    /**
     * Получаем список бирж, которые подключили.
     *
     * @return array
     */
    public function getExternalApi()
    {
        return $this->externalApi;
    }
}