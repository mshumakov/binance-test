<?php

namespace app\models;

/**
 * Interface Exchange
 * @package app\models
 */
interface ExchangeInterface
{
    /**
     * Возвращаем цены с бирж.
     *
     * @return array
     */
    public function getPrices();
}