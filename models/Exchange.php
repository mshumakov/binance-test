<?php

namespace app\models;

/**
 * Class Exchange
 * @package app\models
 */
class Exchange implements ExchangeInterface
{
    /** @var $exchange - ... */
    public $exchange;

    /**
     * Exchange constructor.
     * @param $exchange
     */
    public function __construct($exchange)
    {
        $this->exchange = $exchange;
    }

    /**
     * @inheritDoc
     */
    public function getPrices()
    {
        return [];
    }
}