<?php

return [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'components' => [
        'integrator' => [
            'class' => 'app\components\Integrator',
            'exchanges' => [
                /*
                 '\Binance\API' => [
                    'class' => 'app\models\binance\Binance',
                    'apiKey' => 'secret',
                    'secretKey' => 'secret',
                ],
                 */
            ]
        ],
        'request' => [
            'cookieValidationKey' => 'ynu11G5zSAnmS-KsJ6QPbHY8y2WpEt9q',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
];
